<?php

/**
 * Admin page for the Conflict Resolver
 * 
 * @return An array containing the admin form
 */
function conflict_resolver_admin_settings() {
  $form = array();
  
  $engines = array();
  $raw_engines = module_invoke_all('diff_info');
  if (!empty($raw_engines['name'])) {
    if (is_array($raw_engines['name'])) {
      foreach ($raw_engines['name'] as $idx => $engine) {
        $engines[$engine] = $raw_engines['label'][$idx];
      }
    }
    else {
      $engines[$raw_engines['name']] = $raw_engines['label'];
    }
  }
  
  $form['conflict_resolver_engine'] = array(
    '#type' => 'select',
    '#title' => t('Engine'),
    '#default_value' => variable_get('conflict_resolver_engine', 'xdiff'),
    '#options' => $engines,
    '#description' => t('The engine determines which logic will be used to generate the diffs and patches involved with the merging process.'),
  );
  
  return system_settings_form($form);
}